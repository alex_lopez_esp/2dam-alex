package raf;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;


public class Persistencia {
	RandomAccessFile raf;

	public Persistencia(String nomArchivo) throws FileNotFoundException {
		raf = new RandomAccessFile(new File(nomArchivo), "rw");  
	}

	public void guardar(Cliente cli) throws IOException {		
		raf.writeShort(cli.getId());
        raf.writeChars(cli.getNombre().toString());
        raf.writeChars(cli.getApellidos().toString());
        raf.writeFloat(cli.getSaldo());
	}

	public Cliente leer() throws IOException {
		return leerRegistro();
	}

	public Cliente buscarCliente(short idCliente) throws IOException {
		Cliente cliente;
		raf= new RandomAccessFile("clientes.dat","rw");
		for (int i = 0; i < totalRegistros(); i++) {
			cliente = leer();
			if (cliente.getId()==idCliente) {
				return cliente;
			}
		}
		raf.close();
		return null;
	}

	public long compactarFichero() throws IOException {

		Cliente clientePrueba;
		ArrayList<Cliente> clientes=new ArrayList<>();

		raf=new RandomAccessFile("clientes.dat","rw");
		for (int i = 0; i < totalRegistros(); i++) {
			clientePrueba=leer();
			if (clientePrueba.getId()>0){
				clientes.add(clientePrueba);

			}

		}
		raf.setLength(0);
		for (int x = 0; x < clientes.size(); x++) {

			guardar(clientes.get(x));
			System.out.println(clientes.get(x));
		}
		raf.close();
		return clientes.size();
	}

	private Cliente leerRegistro() throws IOException {
		short id = raf.readShort();        
        String nombre = "";        
        for (int i = 0; i < Cliente.KTNOMBRE; i++) {
            nombre = nombre + raf.readChar();
        } 
        String apellidos = "";
        for (int i = 0; i < Cliente.KTAPELLIDOS; i++) {
            apellidos = apellidos + raf.readChar();
        }
        float saldo = raf.readFloat();
        Cliente cli = new Cliente(id, nombre, apellidos, saldo);        
        return cli;        
	}

	public void irInicio() throws IOException {
		raf.seek(0);		
	}

	public long totalRegistros() throws IOException {
		return raf.length() / Cliente.KTAM;
	}

}
