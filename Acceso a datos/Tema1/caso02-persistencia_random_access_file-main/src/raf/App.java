package raf;

import java.io.IOException;

public class App {
	static Persistencia p;

	public static void main(String[] args) {
		try {
			p = new Persistencia("clientes.dat");
			rellenaFichero();
			muestraFichero();

		} catch (IOException e) {
			System.out.println("Error: " + e.getMessage());
		}


	}

	private static void muestraFichero() throws IOException {
		Cliente cli;
		p.irInicio();
		for (int i = 0; i < p.totalRegistros(); i++) {
			cli = p.leer();
			System.out.println(cli);
		}
		System.out.println("Total registros: " + p.totalRegistros());
	}

	private static void rellenaFichero() {
		try {
			for (int i = 1; i <= 500; i++) {
				int a = ((short) Math.round(Math.random())) == 1 ? i * (-1) : i;
				Cliente cli = new Cliente((short) a, "nomcliente" + i, "apellidoscliente" + i,
						(float) Math.random() * 1000);

				p.guardar(cli);
			}
		} catch (IOException e) {
			System.out.println("Error: " + e.getMessage());
		}

	}

}