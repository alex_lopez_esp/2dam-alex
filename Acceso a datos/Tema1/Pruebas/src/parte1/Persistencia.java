package parte1;

import java.io.*;
import java.util.ArrayList;

public class Persistencia {
    private File f;

    public Persistencia() {
        f=new File("personas.dat");

    }
    public void guardar(Persona persona) throws IOException {
        DataOutputStream dos= new DataOutputStream(new FileOutputStream(f,true));
        dos.writeInt(persona.getId());
        dos.writeUTF(persona.getNombre());
        dos.writeUTF(persona.getApellidos());
        dos.writeUTF(persona.getEdad());
        dos.close();
    }

    public ArrayList leer() throws IOException {
        ArrayList<Persona> listaPersonas=new ArrayList();
        FileInputStream fis=new FileInputStream(f);
        DataInputStream dis= new DataInputStream(fis);
        while (fis.available()>0){
            Persona p=new Persona();
            p.setId(dis.readInt());
            p.setNombre(dis.readUTF());
            p.setApellidos(dis.readUTF());
            p.setEdad(dis.readUTF());
            listaPersonas.add(p);

        }
        dis.close();

        return listaPersonas;
    }
}
