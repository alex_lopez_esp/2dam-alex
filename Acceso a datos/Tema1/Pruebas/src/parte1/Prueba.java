package parte1;

import java.io.*;

public class Prueba {

    public Prueba() throws FileNotFoundException {
    }

    public static void main(String[] args) throws IOException, ClassNotFoundException {
/*        //Te permite crear instancias de ficheros



        //File miArchivo = new File(miHome,"hola.txt");
        //metodo getPath nos dice la ruta de donde se encuentra la carpetaq personal del usuario
        //System.out.println(miArchivo.getPath());

        if (miArchivo.exists()) {
            System.out.println("Existe");
            // .length te dice el tamaño en bytes del archivo .
            /*
            System.out.println("Tamaño del fichero: "+miArchivo.length()+"bytes");
            // getName te dice el nombre del archivo.
            System.out.println("Nombre fichero: "+miArchivo.getName());
            //
            System.out.println("Nombre padre: " +miArchivo.getParent());
            // getAbsolutePath te dice la ruta absoluta con los saltos
            System.out.println("Ruta absoluta: "+miArchivo.getAbsolutePath());

            //getCanonicalPath te dice la ruta absoluta pero sin los saltos
            System.out.println("Ruta canonica: "+miArchivo.getCanonicalPath());


        } else {
            System.out.println("No existe");
            miArchivo.createNewFile();

        }
        if (!miCarpeta.exists()) {
            System.out.println("Carpeta creada");
            miCarpeta.mkdir();
        }

        String[] listaFicheros;
        listaFicheros = miCarpeta.list();

        for (String nombre : listaFicheros) {
            System.out.println(nombre);
        }



        File[] listaFiles =miCarpeta.listFiles();
        for (int i = 0; i < listaFicheros.length-1; i++) {
            if (listaFiles[i].isDirectory()){
                System.out.print("*");
                System.out.println(listaFiles[i].getAbsoluteFile());
            }


        }
     */

        File miHome = new File(System.getProperty("user.home"));
        File miArchivo = new File(miHome, "hola.txt");
        File origen = new File(miHome, "hola.txt");
        File destino = new File(miHome, "hola2.txt");

        // copia(origen, destino);
        //DataOutputStream dos=new DataOutputStream(new FileOutputStream())
/*
        try (FileOutputStream fos = new FileOutputStream(miArchivo,true)) {
            for (int i = 65; i < 91; i++) {
                fos.write(i);
            }
            String cadena="\t\t\tADRI CAMPERO";
            fos.write(cadena.getBytes());
            System.out.println("Escritura Ok");
        } catch (IOException ex) {
            System.err.println("No ha sido posible encontrar el fichero: " + ex.getMessage());
        }finally {
            System.out.println("Fin proceso");
        }
    }
    */



    /*
    //Copiar archivo a otro archivo

    private static void copia(File origen, File destino) {
        try (FileInputStream fis=new FileInputStream(origen);FileOutputStream fos= new FileOutputStream(destino)) {
            byte[] bloque= new byte[(int) origen.length()];
            fis.read(bloque);
            fos.write(bloque);
            System.out.println("Copiado!!");

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
*/


    //Lecturad del fichero



 /*
        try (FileInputStream fis = new FileInputStream(miArchivo)){

            byte[] bloque;
            int leidos;
            while (fis.available()>0){
                bloque =new byte[1000];
                leidos=fis.read(bloque);
                System.out.println("leidos :"+leidos);
            }




        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e){
            System.out.println(e.getMessage());
        }finally {
            System.out.println("ADIOS");

        }





*/

/*
        parte1.Persona alex=new parte1.Persona(1,"Alex","Lopez","20");
        parte1.Persona pedro=new parte1.Persona(2,"Pedro","primo","30");
        parte1.Persistencia persistencia=new parte1.Persistencia();
        persistencia.guardar(alex);
        persistencia.guardar(pedro);
        ArrayList<parte1.Persona> personas= persistencia.leer();
        for (int i = 0; i < personas.size(); i++) {
            System.out.println(personas.get(i).toString());
        }

 */







/*
        //Escritura
        DataOutputStream dos = new DataOutputStream(new FileOutputStream("fichero.dat",true));
        dos.writeInt(100);
        dos.writeBoolean(true);
        dos.writeDouble(32.2332);
        dos.writeUTF("HOLA MUNDO");
        dos.writeInt(200);
        dos.close();

        //Lectura
        FileInputStream fis = new FileInputStream("fichero.dat");
        DataInputStream dis = new DataInputStream(fis);
        while (fis.available()>0) {
            System.out.println(dis.readInt());
            System.out.println(dis.readBoolean());
            System.out.println(dis.readDouble());
            System.out.println(dis.readUTF());
            System.out.println(dis.readInt());
            System.out.println("************************************************");
        }
        dis.close();
        dos.close();
*/

        Persona alex=new Persona(1,"Alex","Lopez","20");
        Persona pedro=new Persona(2,"Pedro","primo","30");
        File f= new File("personas.oos");
        ObjectOutputStream oos=new ObjectOutputStream(new FileOutputStream(f,true));
        oos.writeObject(alex);
        oos.writeObject(pedro);
        oos.close();
        FileInputStream fis= new FileInputStream(f);
        ObjectInputStream ois=new ObjectInputStream(fis);
        Persona p;
        while (fis.available()>0){
            p=(Persona) ois.readObject();
            System.out.println(p);
        }

}
}





