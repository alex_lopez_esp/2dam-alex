import java.io.IOException;
import java.io.RandomAccessFile;

public class Ap4 {
    public static void main(String[] args) throws IOException {
        RandomAccessFile raf=new RandomAccessFile("fichero.raf","rw");
        raf.writeInt(123);//4
        raf.writeDouble(12.34);//8
        raf.close();


        RandomAccessFile raft= new RandomAccessFile("fichero.raf","rw");
        raft.seek(4);
        System.out.println(raft.readDouble());
        raft.seek(raft.length());
        raft.writeInt(321);
        raft.close();
    }
}
