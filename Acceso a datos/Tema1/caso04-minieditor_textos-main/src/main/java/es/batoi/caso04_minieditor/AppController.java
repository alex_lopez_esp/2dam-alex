package es.batoi.caso04_minieditor;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.*;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author sergio
 */
public class AppController implements Initializable {

    @FXML
    private TextArea taEditor;
    @FXML
    private Label lblInfo;
    @FXML
    private Button btnAbrir;
    @FXML
    private Button btnCerrar;
    @FXML
    private Button btnGuardar;
    @FXML
    private Button btnNuevo;

    private Stage escenario;
    private File f;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        taEditor.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValor, String newValor) {
                lblInfo.setText("\t\t\t\t\t\t\t\t\t"+newValor.length()+" caracteres  y "+contarLineas(newValor)+" lineas" );
            }
        });
    }

    @FXML
    private void handleNuevo() {
        try {
            taEditor.setDisable(false);
            if (taEditor.getText().length()<=0 ){
                System.out.println("Has pulsado botón nuevo");

            }else {
                taEditor.setText("");
                handleGuardar();
            }
        }catch (NullPointerException e){
            System.out.println("Has cerrado la  pestaña Nuevo");
        }

    }

    @FXML
    private void handleAbrir() {
        try {
            FileChooser fc = new FileChooser();

            fc.setTitle("ABRIR ARCHIVO");
            f = fc.showOpenDialog(escenario);
            BufferedReader br= null;
            try {
                br = new BufferedReader(new FileReader(f));

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            String linea= null;
            try {
                linea = br.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }

            taEditor.setText("");
            while(linea!=null){
                taEditor.appendText(linea);
                taEditor.appendText("\n");
                try {
                    linea=br.readLine();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
            taEditor.setDisable(false);
        }catch (NullPointerException e){
            System.out.println("Has cerrado la pestaña Abrir  ");
        }

    }

    @FXML
    private void handleGuardar() {
        try {

            FileChooser fc = new FileChooser();
            fc.setTitle("GUARDAR ARCHIVO");
            f = fc.showSaveDialog(escenario);
            BufferedWriter bw= null;
            try {
                bw = new BufferedWriter(new FileWriter(f,true));
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                bw.write(taEditor.getText());
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                bw.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }catch (NullPointerException e){
            System.out.println("Has cerrado la pestaña Guardar");
        }

    }

    @FXML
    private void handleCerrar() {
        try {
            taEditor.setDisable(true);
            handleGuardar();
            taEditor.setText("");
        }catch (NullPointerException ex){
            System.out.println(ex.getMessage());
        }
    }


    void setEscenario(Stage escenario) {
        this.escenario = escenario;
    }

    public static int contarLineas(String linea){
        int contar=0;
        int total=linea.length();
        for (int i = 0; i < total; i++) {
            char letter=linea.charAt(i);
            if (letter== '\n'){
                contar++;
            }
        }
        return contar+1;
    }


}
